<?php

// Routes
/**
 * @api {get} /waiting Request the list of values waiting to be translated
 * @apiName WaitingValues
 * @apiGroup Translation
 * @apiPermission Agent
 */
$app->get('/waiting', \App\Controllers\ValuesController::class.':waitingAction');

/**
 * @api {get} /value/{valueGuideId} Get list of translations of value
 * @apiName TranslatedValues
 * @apiGroup Translation
 * @apiPermission Admin
 *
 * @apiParam {Number} valueGuideId ID
 */
$app->get('/value/{valueGuideId}', \App\Controllers\ValuesController::class.':valueAction');

/**
 * @api {post} /value Sends a value to the translation to be translated
 * @apiName SendToTranslate
 * @apiGroup Translation
 * @apiPermission Admin
 *
 * @apiParam {String} value Value to be translated
 * @apiParam {String} langCode Language code of the primary value
 */
$app->post('/value', \App\Controllers\ValuesController::class.':postValueAction');

/**
 * @api {post} /translate Send translation of value to the translation memory
 * @apiName TranslateValue
 * @apiGroup Translation
 * @apiPermission Agent
 *
 * @apiParam {Number} valueGuideId ID of value
 * @apiParam {String} value Translated value
 * @apiParam {String} langCode Language code of translated value
 */
$app->post('/translate', \App\Controllers\ValuesController::class.':postTranslateAction');

// Auth

/**
 * @api {post} /token Request agent's token
 * @apiName AgentToken
 * @apiGroup Auth
 * @apiPermission none
 *
 * @apiParam {String} username Username or Email
 * @apiParam {String} password password
 * @apiParam {String} accessType Access type (agent, admin)
 */
$app->post('/token', \App\Controllers\AuthController::class.':accessTokenAction');