<?php

namespace App\Repositories;

use App\Contracts\Repository;
use App\Models\ValueGuide;
use PDO;
use Slim\PDO\Statement\SelectStatement;

class ValueGuideRepository extends Repository
{
    /**
     * Get table name.
     *
     * @return string
     */
    protected function getTableName()
    {
        return 'TR_ValueGuides';
    }

    /**
     * @return SelectStatement
     */
    private function getBaseQuery()
    {
        return $this->connection->select([$this->tableName.'.*'])->from($this->tableName);
    }

    /**
     * @param array $filters
     *
     * @return array
     */
    public function getValueGuides($filters = [])
    {
        $query = $this->getBaseQuery();

        if (isset($filters['id'])) {
            $query->where('id', '=', $filters['id']);
        }

        if (isset($filters['isTranslated'])) {
            $query->where('isTranslated', '=', $filters['isTranslated']);
        }

        $statement = $query->execute();

        $results = $statement->fetchAll(PDO::FETCH_CLASS, ValueGuide::class);

        return $results;
    }

    /**
     * @param $id
     *
     * @return ValueGuide
     */
    public function getValuesGuideById($id)
    {
        $query = $this->getBaseQuery()->where('id', '=', $id);
        $statement = $query->execute();

        $result = $statement->fetchObject(ValueGuide::class);

        return $result;
    }

    /**
     * @param $valueText
     * @param $langCode
     *
     * @return array
     */
    public function getValueGuideByValueText($valueText, $langCode)
    {
        $query = $this->getBaseQuery()
            ->leftJoin('TR_Values', 'TR_ValueGuides.id', '=', 'TR_Values.valueGuideId')
            ->where('TR_Values.text', '=', $valueText)
            ->where('TR_Values.langCode', '=', $langCode)
            ->where('TR_Values.isPrimary', '=', 1);

        $statement = $query->execute();

        $result = $statement->fetchObject(ValueGuide::class);

        return $result;
    }
}
