<?php

namespace App\Repositories;

use App\Contracts\Repository;
use App\Models\Value;
use PDO;
use Slim\PDO\Statement\SelectStatement;

class ValuesRepository extends Repository
{
    /**
     * Get table name.
     *
     * @return string
     */
    protected function getTableName()
    {
        return 'TR_Values';
    }

    /**
     * @return SelectStatement
     */
    private function getBaseQuery()
    {
        return $this->connection->select(['*'])->from($this->tableName);
    }

    /**
     * @return Value[]
     */
    public function getValues()
    {
        $query = $this->getBaseQuery();
        $statement = $query->execute();

        $results = $statement->fetchAll(PDO::FETCH_CLASS, Value::class);

        return $results;
    }

    /**
     * @param $id
     *
     * @return Value
     */
    public function getValueById($id)
    {
        $query = $this->getBaseQuery()->where('id', '=', $id);
        $statement = $query->execute();

        $result = $statement->fetchObject(Value::class);

        return $result;
    }

    /**
     * @param $valueGuideId
     *
     * @return Value[]
     */
    public function getValuesByGuideId($valueGuideId)
    {
        $query = $this->getBaseQuery()->where('valueGuideId', '=', $valueGuideId);
        $statement = $query->execute();

        $results = $statement->fetchAll(PDO::FETCH_CLASS, Value::class);

        return $results;
    }
}
