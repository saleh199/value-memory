<?php

namespace App\Repositories;

use App\Contracts\Repository;
use App\Models\Agent;
use PDO;
use Slim\PDO\Statement\SelectStatement;

class AgentsRepository extends Repository
{
    /**
     * Get table name.
     *
     * @return string
     */
    protected function getTableName()
    {
        return 'TR_Agents';
    }

    /**
     * @return SelectStatement
     */
    public function getBaseQuery()
    {
        return $this->connection->select(['*'])->from($this->tableName);
    }

    /**
     * Get List of agents.
     *
     * @return array
     */
    public function getAgents()
    {
        $query = $this->getBaseQuery();
        $statement = $query->execute();
        $results = $statement->fetchAll(PDO::FETCH_CLASS, Agent::class);

        return $results;
    }

    /**
     * Get agent by ID.
     *
     * @param $id
     *
     * @return Agent
     */
    public function getAgentById($id)
    {
        $query = $this->getBaseQuery()->where('id', '=', $id);
        $statement = $query->execute();

        $result = $statement->fetchObject(Agent::class);

        return $result;
    }

    /**
     * @param $email
     *
     * @return Agent|bool
     */
    public function getAgentByEmail($email)
    {
        $query = $this->getBaseQuery()
            ->where('email', '=', $email);
        $statement = $query->execute();
        $result = $statement->fetchObject(Agent::class);

        return $result;
    }

    /**
     * @param $username
     *
     * @return Agent|bool
     */
    public function getAgentByUserName($username)
    {
        $query = $this->getBaseQuery()
            ->where('userName', '=', $username);
        $statement = $query->execute();
        $result = $statement->fetchObject(Agent::class);

        return $result;
    }
}
