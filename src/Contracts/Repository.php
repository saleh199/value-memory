<?php

namespace App\Contracts;

use Slim\PDO\Database as PDO;

abstract class Repository
{
    /** @var $connection PDO */
    protected $connection;

    /** @var $tableName string */
    protected $tableName;

    /**
     * Repository constructor.
     *
     * @param PDO $databaseConnection
     */
    public function __construct(PDO $databaseConnection)
    {
        $this->connection = $databaseConnection;
        $this->tableName = $this->getTableName();
    }

    /**
     * Set table name.
     *
     * @return string
     *
     * @throws \Exception
     */
    protected function getTableName()
    {
        throw new \Exception('No table name defined');
    }
}
