<?php

namespace App\Contracts;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

class BaseController
{
    /** @var $container ContainerInterface */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $data mixed
     *
     * @return ResponseInterface
     */
    public function json($data)
    {
        return $this->container->get('response')->withJson(['data' => $data]);
    }
}
