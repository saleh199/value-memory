<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        // Database configurations
        'db' => [
            'default' => [
                'host' => '127.0.0.1',
                'port' => '3307',
                'dbname' => 'sony_translation',
                'username' => 'root',
                'password' => 'root',
                'charset' => 'utf8'
            ],
            'test' => [
                'host' => '127.0.0.1',
                'port' => '3307',
                'dbname' => 'translation_test',
                'username' => 'root',
                'password' => 'root',
                'charset' => 'utf8'
            ]        ]
    ],
];
