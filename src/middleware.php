<?php

// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

$container = $app->getContainer();

// Add middleware for agencies
$app->add(new Slim\Middleware\JwtAuthentication([
    'path' => '/',
    'passthrough' => ['/token'],
    'secret' => '1234567890',
    'error' => function ($request, $response, $args) {
        $data['result'] = $args['message'];

        return $response->withJson($data);
    },
    'callback' => function ($request, $response, $args) use ($container) {
        $container['token'] = $args['decoded'];
    },
]));
