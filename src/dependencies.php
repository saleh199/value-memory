<?php

// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];

    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));

    return $logger;
};

$container['db'] = function ($c) {
    $env = $_ENV['env'] ? $_ENV['env'] : 'default';
    $settings = $c->get('settings')['db'][$env];

    $dsn = "mysql:host=$settings[host];port=$settings[port];dbname=$settings[dbname];charset=$settings[charset]";

    $db = new Slim\PDO\Database($dsn, $settings['username'], $settings['password']);

    return $db;
};

$container['AgentsRepository'] = function ($c) {
    return new App\Repositories\AgentsRepository($c->get('db'));
};

$container['ValueGuideRepository'] = function ($c) {
    return new App\Repositories\ValueGuideRepository($c->get('db'));
};

$container['ValuesRepository'] = function ($c) {
    return new App\Repositories\ValuesRepository($c->get('db'));
};

$container['ValueGuidesService'] = function ($c) {
    return new \App\Services\ValueGuidesService($c, $c->get('db'));
};

$container['AuthService'] = function ($c) {
    return new \App\Services\AuthService($c);
};

$container['ValuesService'] = function ($c) {
    return new \App\Services\ValuesService($c, $c->get('db'));
};
