<?php

namespace App\Models;

use App\Contracts\Model;

class Value extends Model
{
    /** @var $id int */
    private $id;

    /** @var $valueGuideId int */
    private $valueGuideId;

    /** @var $text string */
    private $text;

    /** @var $langCode string */
    private $langCode;

    /** @var $createdAt string */
    private $createdAt;

    /** @var $agentId int */
    private $agentId;

    /** @var $isPrimary bool */
    private $isPrimary;

    /**
     * Value constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getValueGuideId()
    {
        return $this->valueGuideId;
    }

    /**
     * @param int $valueGuideId
     */
    public function setValueGuideId($valueGuideId)
    {
        $this->valueGuideId = $valueGuideId;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getLangCode()
    {
        return $this->langCode;
    }

    /**
     * @param string $langCode
     */
    public function setLangCode($langCode)
    {
        $this->langCode = $langCode;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getAgentId()
    {
        return $this->agentId;
    }

    /**
     * @param int $agentId
     */
    public function setAgentId($agentId)
    {
        $this->agentId = $agentId;
    }

    /**
     * @return bool
     */
    public function getIsPrimary()
    {
        return $this->isPrimary;
    }

    /**
     * @param bool $isPrimary
     */
    public function setIsPrimary($isPrimary)
    {
        $this->isPrimary = $isPrimary;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'text' => $this->getText(),
            'isPrimary' => $this->getIsPrimary(),
            'langCode' => $this->getLangCode(),
            'valueGuideId' => $this->getValueGuideId(),
            'agentId' => $this->getAgentId(),
            'createdAt' => $this->getCreatedAt(),
        ];
    }
}
