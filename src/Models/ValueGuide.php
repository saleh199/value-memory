<?php

namespace App\Models;

use App\Contracts\Model;

class ValueGuide extends Model
{
    /** @var $id int */
    private $id;

    /** @var $requiredLangCode string */
    private $requiredLangCode;

    /** @var $createdAt string */
    private $createdAt;

    /** @var $isTranslated bool */
    private $isTranslated;

    /** @var $values Value[] */
    private $values;

    /**
     * ValueGuide constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getRequiredLangCode()
    {
        return $this->requiredLangCode;
    }

    /**
     * @param string $requiredLangCode
     */
    public function setRequiredLangCode($requiredLangCode)
    {
        $this->requiredLangCode = $requiredLangCode;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return bool
     */
    public function getIsTranslated()
    {
        return $this->isTranslated;
    }

    /**
     * @param bool $isTranslated
     */
    public function setIsTranslated($isTranslated)
    {
        $this->isTranslated = $isTranslated;
    }

    /**
     * @return Value[]
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param Value[] $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'requiredLangCode' => $this->getRequiredLangCode(),
            'createdAt' => $this->getCreatedAt(),
            'isTranslated' => $this->getIsTranslated(),
            'values' => $this->getValues(),
        ];
    }
}
