<?php

namespace App\Services;

use App\Contracts\Service;
use App\Models\Agent;
use Firebase\JWT\JWT;
use Tuupola\Base62;

class AuthService extends Service
{
    /**
     * @param Agent $agent
     *
     * @return string
     */
    public function generateAgentToken(Agent $agent)
    {
        return $this->generateToken($agent->getId());
    }

    /**
     * @param $sub
     *
     * @return string
     */
    public function generateToken($sub)
    {
        $now = new \DateTime();
        $future = new \DateTime('now +2 months');
        $jti = Base62::encode(random_bytes(16));

        $payload = [
            'iat' => $now->getTimestamp(),
            'exp' => $future->getTimestamp(),
            'jti' => $jti,
            'sub' => $sub,
        ];

        $secret = '1234567890';

        $token = JWT::encode($payload, $secret, 'HS256');

        return $token;
    }
}
