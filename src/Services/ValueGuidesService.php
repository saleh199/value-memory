<?php

namespace App\Services;

use App\Contracts\Service;
use App\Models\ValueGuide;
use App\Repositories\ValueGuideRepository;
use App\Repositories\ValuesRepository;
use Interop\Container\ContainerInterface;
use Slim\PDO\Database as PDO;

class ValueGuidesService extends Service
{
    /**
     * @var PDO
     */
    private $connection;

    public function __construct(ContainerInterface $container, PDO $connection)
    {
        parent::__construct($container);
        $this->connection = $connection;
    }

    /**
     * @param $filters
     *
     * @return array
     */
    public function getValueGuides($filters = [])
    {
        $scopes = [];

        /** @var ValueGuideRepository $valueGuideRepository */
        $valueGuideRepository = $this->container->get('ValueGuideRepository');

        if (isset($filters['scopes'])) {
            $scopes = $filters['scopes'];
            unset($filters['scopes']);
        }

        $results = $valueGuideRepository->getValueGuides($filters);

        if (in_array('getValues', $scopes)) {
            /** @var ValuesRepository $valuesRepository */
            $valuesRepository = $this->container->get('ValuesRepository');

            array_walk($results, function (&$valueGuide) use ($valuesRepository) {
                /* @var ValueGuide $valueGuide */
                $values = $valuesRepository->getValuesByGuideId($valueGuide->getId());

                $valueGuide->setValues($values);
            });
        }

        return $results;
    }

    /**
     * @param $requiredLangCode
     *
     * @return string
     */
    public function createValueGuide($requiredLangCode)
    {
        $statement = $this->connection->insert(['requiredLangCode', 'isTranslated', 'createdAt'])
            ->into('TR_ValueGuides')
            ->values([$requiredLangCode, 0, (new \DateTime())->format('Y-m-d H:i:s')]);

        return $statement->execute();
    }

    /**
     * @param $valueGuideId
     *
     * @return int
     */
    public function markAsTranslated($valueGuideId)
    {
        $statement = $this->connection
            ->update(['isTranslated' => 1])
            ->table('TR_ValueGuides')
            ->where('id', '=', $valueGuideId);

        return $statement->execute();
    }
}
