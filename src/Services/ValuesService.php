<?php

namespace App\Services;

use App\Contracts\Service;
use Interop\Container\ContainerInterface;
use PDO;

class ValuesService extends Service
{
    /**
     * @var PDO
     */
    private $connection;

    public function __construct(ContainerInterface $container, PDO $connection)
    {
        parent::__construct($container);
        $this->connection = $connection;
    }

    /**
     * @param $valueGuideId
     * @param $text
     * @param $langCode
     * @param $isPrimary
     * @param $agentId
     *
     * @return mixed
     */
    public function createValue($valueGuideId, $text, $langCode, $isPrimary, $agentId)
    {
        $statement = $this->connection->insert(['valueGuideId', 'text', 'langCode', 'createdAt', 'isPrimary', 'agentId'])
            ->into('TR_Values')
            ->values([$valueGuideId, $text, $langCode, (new \DateTime())->format('Y-m-d H:i:s'), $isPrimary, $agentId]);

        return $statement->execute();
    }
}
