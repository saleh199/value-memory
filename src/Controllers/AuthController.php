<?php

namespace App\Controllers;

use App\Contracts\BaseController;
use App\Repositories\AgentsRepository;
use App\Services\AuthService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class AuthController extends BaseController
{
    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     * @param $args
     *
     * @return ResponseInterface
     */
    public function accessTokenAction(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $requestBody = $request->getParsedBody();

        if (!isset($requestBody['username'])) {
            return $response->withStatus(400)->withJson(['data' => [], 'error' => 'required \'username\' parameter']);
        }

        if (!isset($requestBody['password'])) {
            return $response->withStatus(400)->withJson(['data' => [], 'error' => 'required \'password\' parameter']);
        }

        if (!isset($requestBody['accessType'])) {
            return $response->withStatus(400)->withJson(['data' => [], 'error' => 'required \'accessType\' parameter']);
        }

        /** @var AuthService $authService */
        $authService = $this->container->get('AuthService');

        if ($requestBody['accessType'] == 'agent') {
            /** @var AgentsRepository $agentsRepository */
            $agentsRepository = $this->container->get('AgentsRepository');

            if (false !== strpos($requestBody['username'], '@')) { // Login by email
                $agent = $agentsRepository->getAgentByEmail($requestBody['username']);
            } else { // Login by username
                $agent = $agentsRepository->getAgentByUserName($requestBody['username']);
            }

            if ($agent) {
                if (password_verify($requestBody['password'], $agent->getPassword())) {
                    $token = $authService->generateAgentToken($agent);

                    return $this->json(['access_token' => $token]);
                }
            }
        } elseif ($requestBody['accessType'] == 'admin') {
            if ($requestBody['username'] === 'admin' && $requestBody['password'] == 'admin') {
                $token = $authService->generateToken('admin');

                return $this->json(['access_token' => $token]);
            }
        }

        return $response->withStatus(401)->withJson(['data' => [], 'error' => 'unauthorized']);
    }
}
