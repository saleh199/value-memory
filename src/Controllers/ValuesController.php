<?php

namespace App\Controllers;

use App\Contracts\BaseController;
use App\Models\ValueGuide;
use App\Repositories\ValueGuideRepository;
use App\Repositories\ValuesRepository;
use App\Services\ValueGuidesService;
use App\Services\ValuesService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ValuesController extends BaseController
{
    /**
     * Get the list of values waiting to be translated.
     *
     * @route /waiting
     *
     * @method GET
     *
     * @param $request ServerRequestInterface
     * @param $response ResponseInterface
     * @param $args
     *
     * @return ResponseInterface
     */
    public function waitingAction(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $filters = ['scopes' => ['getValues']];
        $filters['isTranslated'] = 0;

        /** @var ValueGuidesService $valueGuidesService */
        $valueGuidesService = $this->container->get('ValueGuidesService');

        $results = $valueGuidesService->getValueGuides($filters);

        return $this->json($results);
    }

    /**
     * Get the list of translations of value.
     *
     * @route /value
     *
     * @method GET
     *
     * @param $request
     * @param $response
     * @param $args
     *
     * @return ResponseInterface
     */
    public function valueAction(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        if ($this->container->get('token')->sub != 'admin') {
            return $response->withStatus(401)->withJson(['result' => 'unauthorized']);
        }

        $valueGuideId = $args['valueGuideId'];

        /** @var ValuesRepository $valuesRepository */
        $valuesRepository = $this->container->get('ValuesRepository');

        $results = $valuesRepository->getValuesByGuideId($valueGuideId);

        return $this->json($results);
    }

    /**
     * Send value to translation memory to be translated.
     *
     * @route /value
     *
     * @method POST
     *
     * @param $request
     * @param $response
     * @param $args
     *
     * @return static
     */
    public function postValueAction(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        if ($this->container->get('token')->sub != 'admin') {
            return $response->withStatus(401)->withJson(['result' => 'unauthorized']);
        }

        // Convert JSON, XML and URL-encoded data into a native PHP format (array)
        $requestBody = $request->getParsedBody();

        if (!isset($requestBody['value'])) {
            return $response->withStatus(400)->withJson(['data' => [], 'error' => 'required \'value\' parameter']);
        }

        if (!isset($requestBody['langCode'])) {
            return $response->withStatus(400)->withJson(['data' => [], 'error' => 'required \'langCode\' parameter']);
        }

        if (!isset($requestBody['requiredLangCode'])) {
            return $response->withStatus(400)->withJson(['data' => [], 'error' => 'required \'requiredLangCode\' parameter']);
        }

        /** @var ValueGuideRepository $valueGuideRepository */
        $valueGuideRepository = $this->container->get('ValueGuideRepository');

        /** @var ValueGuidesService $valueGuidesService */
        $valueGuidesService = $this->container->get('ValueGuidesService');

        /** @var ValueGuide|bool $result */
        $result = $valueGuideRepository->getValueGuideByValueText($requestBody['value'], $requestBody['langCode']);

        if (!$result) { // if value not found then create new record for value
            /** @var ValuesService $valuesService */
            $valuesService = $this->container->get('ValuesService');

            $valueGuideId = $valueGuidesService->createValueGuide($requestBody['requiredLangCode']);

            $valuesService->createValue($valueGuideId, $requestBody['value'], $requestBody['langCode'], 1, null);
        } else {
            $valueGuideId = $result->getId();
        }

        $results = $valueGuidesService->getValueGuides(['id' => $valueGuideId, 'scopes' => ['getValues']]);

        return $this->json($results[0]);
    }

    /**
     * Send translation of a value.
     *
     * @route /translate
     *
     * @method POST
     *
     * @param $request
     * @param $response
     * @param $args
     */
    public function postTranslateAction(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        // Convert JSON, XML and URL-encoded data into a native PHP format (array)
        $requestBody = $request->getParsedBody();

        if (!isset($requestBody['valueGuideId'])) {
            return $response->withStatus(400)->withJson(['data' => [], 'error' => 'required \'valueGuideId\' parameter']);
        }

        if (!isset($requestBody['value'])) {
            return $response->withStatus(400)->withJson(['data' => [], 'error' => 'required \'value\' parameter']);
        }

        if (!isset($requestBody['langCode'])) {
            return $response->withStatus(400)->withJson(['data' => [], 'error' => 'required \'langCode\' parameter']);
        }

        /** @var ValueGuideRepository $valueGuideRepository */
        $valueGuideRepository = $this->container->get('ValueGuideRepository');

        $valueGuide = $valueGuideRepository->getValuesGuideById($requestBody['valueGuideId']);

        if (!$valueGuide) {
            return $response->withStatus(404)->withJson(['data' => [], 'error' => 'Value Guide not found']);
        }

        if ($valueGuide->getIsTranslated()) {
            return $response->withStatus(400)->withJson(['data' => [], 'error' => 'The Value is already translated']);
        }

        /** @var ValuesService $valuesService */
        $valuesService = $this->container->get('ValuesService');

        /** @var ValueGuidesService $valueGuidesServices */
        $valueGuidesServices = $this->container->get('ValueGuidesService');

        $valuesService->createValue(
            $requestBody['valueGuideId'],
            $requestBody['value'],
            $requestBody['langCode'],
            0,
            ($this->container->get('token')->sub != 'admin' ? null : $this->container->get('token')->sub));

        $valueGuidesServices->markAsTranslated($requestBody['valueGuideId']);

        return $response->withStatus(200)->withJson(['success' => true]);
    }
}
