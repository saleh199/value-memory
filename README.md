## Installing / Getting started

A quick introduction of the setup you need to get a running.

- Clone repository to the local:
```shell
$ git clone https://bitbucket.org/saleh199/value-memory.git
```

- Open `src/settings.php` and update default and test database settings.

- Import database from `db/translation.sql` script

- Install dependencies:
``` shell
$ composer install
```

- Run built-in web server:
``` shell
$ php -S 0.0.0.0:8080 -t public public/index.php
```

## Demo data

agency's credentials:
```
username: saleh
password: 12345678
accessType: agent
```

admin's credentials:
```
username: admin
password: admin
accessType: admin
```

## API Documentation

You can see in `apidoc/index.html` directory

Generate API Documentation:
```shell
$ npm install -g apidoc
$ apidoc -i src/ -o apidoc/
```

## Test and Code Coverage
Run test:
```shell
$ php vendor/bin/phpunit
```

This command will create test database according to db configuration on `src/settings.php`

You can find code coverage report after run phpunit in `tests/_reports/` directory.