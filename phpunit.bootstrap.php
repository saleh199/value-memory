<?php

require 'vendor/autoload.php';

if (!in_array('--no-reload-db', $_SERVER['argv'])) {
    $env = $_ENV['env'] ? $_ENV['env'] : 'default';
    $settings = require 'src/settings.php';
    $settings = $settings['settings'];
    $dbConfig = $settings['db'][$env];

    $dsn = "mysql:host=$dbConfig[host];port=$dbConfig[port];charset=$dbConfig[charset]";
    $dbConnection = new PDO($dsn, $dbConfig['username'], $dbConfig['password']);

    $dbConnection->exec('DROP DATABASE IF EXISTS '.$dbConfig['dbname']);
    $dbConnection->exec(sprintf('CREATE DATABASE `%s` DEFAULT CHARACTER SET = `utf8`', $dbConfig['dbname']));
    $dbConnection->exec('SET GLOBAL SQL_MODE=\'ALLOW_INVALID_DATES\'');

    $sqlFile = __DIR__.'/tests/translation.sql';

    passthru(sprintf('mysql --host=%s --port=%s --user=%s --password=%s --database=%s < %s', $dbConfig['host'], $dbConfig['port'], $dbConfig['username'], $dbConfig['password'], $dbConfig['dbname'], $sqlFile));
}
