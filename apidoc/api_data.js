define({ "api": [
  {
    "type": "post",
    "url": "/token",
    "title": "Request agent's token",
    "name": "AgentToken",
    "group": "Auth",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username or Email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "accessType",
            "description": "<p>Access type (agent, admin)</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/routes.php",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/value",
    "title": "Sends a value to the translation to be translated",
    "name": "SendToTranslate",
    "group": "Translation",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "value",
            "description": "<p>Value to be translated</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "langCode",
            "description": "<p>Language code of the primary value</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/routes.php",
    "groupTitle": "Translation"
  },
  {
    "type": "post",
    "url": "/translate",
    "title": "Send translation of value to the translation memory",
    "name": "TranslateValue",
    "group": "Translation",
    "permission": [
      {
        "name": "Agent"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "valueGuideId",
            "description": "<p>ID of value</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "value",
            "description": "<p>Translated value</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "langCode",
            "description": "<p>Language code of translated value</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/routes.php",
    "groupTitle": "Translation"
  },
  {
    "type": "get",
    "url": "/value/{valueGuideId}",
    "title": "Get list of translations of value",
    "name": "TranslatedValues",
    "group": "Translation",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "valueGuideId",
            "description": "<p>ID</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/routes.php",
    "groupTitle": "Translation"
  },
  {
    "type": "get",
    "url": "/waiting",
    "title": "Request the list of values waiting to be translated",
    "name": "WaitingValues",
    "group": "Translation",
    "permission": [
      {
        "name": "Agent"
      }
    ],
    "version": "0.0.0",
    "filename": "src/routes.php",
    "groupTitle": "Translation"
  }
] });
