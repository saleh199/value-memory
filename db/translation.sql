-- MySQL dump 10.13  Distrib 5.7.12, for linux-glibc2.5 (x86_64)
--
-- Host: 127.0.0.1    Database: sony_translation
-- ------------------------------------------------------
-- Server version	5.6.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `TR_Agents`
--

DROP TABLE IF EXISTS `TR_Agents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TR_Agents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `userName` varchar(45) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`userName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TR_Agents`
--

LOCK TABLES `TR_Agents` WRITE;
/*!40000 ALTER TABLE `TR_Agents` DISABLE KEYS */;
INSERT INTO `TR_Agents` VALUES (1,'Saleh','saleh','$2y$10$MNCba8ikt66mcgb3pVSx1O.uF4Hww/wlRX3kZwDHBRGxr8cehxzVa','saleh.saiid@gmail.com','2016-12-29 22:56:42');
/*!40000 ALTER TABLE `TR_Agents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TR_ValueGuides`
--

DROP TABLE IF EXISTS `TR_ValueGuides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TR_ValueGuides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requiredLangCode` varchar(2) NOT NULL,
  `isTranslated` varchar(45) DEFAULT NULL,
  `createdAt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TR_ValueGuides`
--

LOCK TABLES `TR_ValueGuides` WRITE;
/*!40000 ALTER TABLE `TR_ValueGuides` DISABLE KEYS */;
INSERT INTO `TR_ValueGuides` VALUES (1,'tr','0','2016-12-29 23:32:47'),(17,'en','1','2017-01-02 21:34:08'),(18,'en','1','2017-01-02 21:34:08'),(19,'en','0','2017-01-02 21:34:09'),(20,'en','0','2017-01-02 21:34:10'),(21,'en','0','2017-01-02 21:34:11'),(22,'en','0','2017-01-02 21:35:06'),(23,'en','0','2017-01-02 21:36:08'),(24,'en','0','2017-01-02 22:20:15');
/*!40000 ALTER TABLE `TR_ValueGuides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TR_Values`
--

DROP TABLE IF EXISTS `TR_Values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TR_Values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valueGuideId` int(11) NOT NULL,
  `text` longtext CHARACTER SET utf8 NOT NULL,
  `isPrimary` tinyint(1) DEFAULT NULL,
  `langCode` varchar(2) NOT NULL,
  `agentId` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Values_1_idx` (`agentId`),
  KEY `fk_Values_1_idx1` (`valueGuideId`),
  KEY `langCode_idx` (`langCode`),
  CONSTRAINT `FK_Values_Agents` FOREIGN KEY (`agentId`) REFERENCES `TR_Agents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Values_ValueGuides` FOREIGN KEY (`valueGuideId`) REFERENCES `TR_ValueGuides` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TR_Values`
--

LOCK TABLES `TR_Values` WRITE;
/*!40000 ALTER TABLE `TR_Values` DISABLE KEYS */;
INSERT INTO `TR_Values` VALUES (1,1,'saleh',1,'en',1,'2017-01-01 14:56:36'),(15,17,'omar',1,'tr',NULL,'2017-01-02 21:34:08'),(18,17,'omar',0,'tr',NULL,'2017-01-02 23:06:43'),(24,18,'Merhaba',1,'tr',NULL,'2017-01-03 19:31:22'),(25,19,'lütfen',1,'tr',NULL,'2017-01-03 19:31:22'),(26,20,'teşekkürler',1,'tr',NULL,'2017-01-03 19:31:22'),(27,21,'nasıl',1,'tr',NULL,'2017-01-03 19:31:22'),(28,22,'imdat',1,'tr',NULL,'2017-01-03 19:31:22'),(29,23,'Ne demek ?',1,'tr',NULL,'2017-01-03 19:31:22'),(30,24,'Evet',1,'tr',NULL,'2017-01-03 19:31:22'),(31,18,'Hello',0,'en',1,'2017-01-03 19:37:31');
/*!40000 ALTER TABLE `TR_Values` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-03 23:08:07
