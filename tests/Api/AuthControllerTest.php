<?php

namespace Tests\Api;

use Tests\BaseTestCase;

class AuthControllerTest extends BaseTestCase
{
    public function testLoginAsAgencyByUserName()
    {
        $data = [
            'username' => 'saleh',
            'password' => '12345678',
            'accessType' => 'agent',
        ];

        $response = $this->runApp('POST', '/token', $data);

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testLoginAsAgencyByEmail()
    {
        $data = [
            'username' => 'saleh.saiid@gmail.com',
            'password' => '12345678',
            'accessType' => 'agent',
        ];

        $response = $this->runApp('POST', '/token', $data);

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testLoginAsAdmin()
    {
        $data = [
            'username' => 'admin',
            'password' => 'admin',
            'accessType' => 'admin',
        ];

        $response = $this->runApp('POST', '/token', $data);

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testWrongAgencyCredentials()
    {
        $data = [
            'username' => 'wrongusername',
            'password' => 'wrongpassword',
            'accessType' => 'agent',
        ];

        $response = $this->runApp('POST', '/token', $data);

        $this->assertEquals(401, $response->getStatusCode());
    }

    public function testUnauthorizedRequest()
    {
        $response = $this->runApp('POST', '/test');

        $this->assertEquals(401, $response->getStatusCode());
    }

    /**
     * @dataProvider requiredParametersDataProvider
     */
    public function testRequiredParameters($data)
    {
        $response = $this->runApp('POST', '/token', $data);

        $this->assertEquals(400, $response->getStatusCode());
    }

    public function requiredParametersDataProvider()
    {
        return [
            [['password' => '12345', 'accessType' => 'admin']],
            [['username' => 'saleh', 'accessType' => 'admin']],
            [['username' => 'ssss', 'password' => 'ssss']],
        ];
    }
}
