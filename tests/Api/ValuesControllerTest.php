<?php

namespace Tests\Api;

use Tests\BaseTestCase;

class ValuesControllerTest extends BaseTestCase
{
    public function testGetListOfWaitingValues()
    {
        $access_token = $this->loginAsAgency();

        $headers = ['HTTP_AUTHORIZATION' => 'Bearer '.$access_token];

        $response = $this->runApp('GET', '/waiting', [], $headers);
        $results = json_decode($response->getBody(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('data', $results);
        $this->assertGreaterThan(0, count($results['data']));

        foreach ($results['data'] as $value) {
            $this->assertEquals(0, $value['isTranslated']);
        }
    }

    public function testGetListOfTranslationOfValue()
    {
        $valueGuideId = 1;
        $access_token = $this->loginAsAdmin();

        $headers = ['HTTP_AUTHORIZATION' => 'Bearer '.$access_token];

        $response = $this->runApp('GET', '/value/'.$valueGuideId, [], $headers);
        $results = json_decode($response->getBody(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('data', $results);
        $this->assertTrue(is_array($results['data']));
    }

    public function testSendNewValueToBeTranslated()
    {
        $access_token = $this->loginAsAdmin();

        $data = [
            'value' => 'kim',
            'langCode' => 'tr',
            'requiredLangCode' => 'en',
        ];

        $headers = ['HTTP_AUTHORIZATION' => 'Bearer '.$access_token];

        $response = $this->runApp('POST', '/value', $data, $headers);
        $results = json_decode($response->getBody(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('data', $results);
        $this->assertEquals(0, $results['data']['isTranslated']);
    }

    public function testSendOldValueToBeTranslated()
    {
        $access_token = $this->loginAsAdmin();

        $data = [
            'value' => 'imdat',
            'langCode' => 'tr',
            'requiredLangCode' => 'en',
        ];

        $headers = ['HTTP_AUTHORIZATION' => 'Bearer '.$access_token];

        $response = $this->runApp('POST', '/value', $data, $headers);
        $results = json_decode($response->getBody(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('data', $results);
        $this->assertEquals(0, $results['data']['isTranslated']);
    }

    public function testSendOldValueToBeTranslatedButAlreadyTranslated()
    {
        $access_token = $this->loginAsAdmin();

        $data = [
            'value' => 'Merhaba',
            'langCode' => 'tr',
            'requiredLangCode' => 'en',
        ];

        $headers = ['HTTP_AUTHORIZATION' => 'Bearer '.$access_token];

        $response = $this->runApp('POST', '/value', $data, $headers);
        $results = json_decode($response->getBody(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('data', $results);
        $this->assertEquals(1, $results['data']['isTranslated']);
    }

    /**
     * @dataProvider translateValueDataProvider
     */
    public function testSendTranslationOfValue($data)
    {
        $access_token = $this->loginAsAgency();
        $headers = ['HTTP_AUTHORIZATION' => 'Bearer '.$access_token];

        $response = $this->runApp('POST', '/translate', $data, $headers);
        $results = json_decode($response->getBody(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('success', $results);
        $this->assertEquals(true, $results['success']);
    }

    public function translateValueDataProvider()
    {
        return [
            [['valueGuideId' => 24, 'value' => 'Yes', 'langCode' => 'en']],
            [['valueGuideId' => 23, 'value' => 'What it means?', 'langCode' => 'en']],
            [['valueGuideId' => 20, 'value' => 'Thank you', 'langCode' => 'en']],
        ];
    }

    public function testSendTranslationForValueAlreadyTranslated()
    {
        $access_token = $this->loginAsAgency();
        $headers = ['HTTP_AUTHORIZATION' => 'Bearer '.$access_token];

        $data = ['valueGuideId' => 18, 'value' => 'Hello', 'langCode' => 'en'];

        $response = $this->runApp('POST', '/translate', $data, $headers);
        $results = json_decode($response->getBody(), true);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertArrayHasKey('error', $results);
    }
}
