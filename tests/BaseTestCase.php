<?php

namespace Tests;

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Environment;

/**
 * This is an example class that shows how you could set up a method that
 * runs the application. Note that it doesn't cover all use-cases and is
 * tuned to the specifics of this skeleton app, so if your needs are
 * different, you'll need to change it.
 */
class BaseTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * Use middleware when running application?
     *
     * @var bool
     */
    protected $withMiddleware = true;

    /**
     * Process the application given a request method and URI.
     *
     * @param string            $requestMethod the request method (e.g. GET, POST, etc.)
     * @param string            $requestUri    the request URI
     * @param array|object|null $requestData   the request data
     * @param array|null        $headers       thw request headers
     *
     * @return Response
     */
    public function runApp($requestMethod, $requestUri, $requestData = null, $headers = null)
    {
        $environmentParameters = [
            'REQUEST_METHOD' => $requestMethod,
            'REQUEST_URI' => $requestUri,
        ];

        if ($headers) {
            $environmentParameters = array_merge($environmentParameters, $headers);
        }

        // Create a mock environment for testing with
        $environment = Environment::mock(
            $environmentParameters
        );

        // Set up a request object based on the environment
        $request = Request::createFromEnvironment($environment);

        // Add request data, if it exists
        if (isset($requestData)) {
            $request = $request->withParsedBody($requestData);
        }

        // Set up a response object
        $response = new Response();

        // Use the application settings
        $settings = require __DIR__.'/../src/settings.php';

        // Instantiate the application
        $app = new App($settings);

        // Set up dependencies
        require __DIR__.'/../src/dependencies.php';

        // Register middleware
        if ($this->withMiddleware) {
            require __DIR__.'/../src/middleware.php';
        }

        // Register routes
        require __DIR__.'/../src/routes.php';

        // Process the application
        $response = $app->process($request, $response);

        // Return the response
        return $response;
    }

    public function loginAsAgency()
    {
        $data = [
            'username' => 'saleh',
            'password' => '12345678',
            'accessType' => 'agent',
        ];

        $response = $this->runApp('POST', '/token', $data);

        $result = json_decode($response->getBody(), true);

        return $result['data']['access_token'];
    }

    public function loginAsAdmin()
    {
        $data = [
            'username' => 'admin',
            'password' => 'admin',
            'accessType' => 'admin',
        ];

        $response = $this->runApp('POST', '/token', $data);

        $result = json_decode($response->getBody(), true);

        return $result['data']['access_token'];
    }
}
